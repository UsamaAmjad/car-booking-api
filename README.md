# Simple REST API for Car Booking
 **Author:** [Usama Amjad](https://stackoverflow.com/users/4704510/)  
 
 A simple Car Booking API demonstration built using `Spring Boot 2`, `Spring MVC`, `Spring Data JPA`, and `H2 DB`.
 
 ## Highlights 
 
 - MVC Architecture
 - Docker Build
 - Spring Profiles
 - Executable jar file generation
 - Testing with JUnit and Mockito
 - JPA Repositories 
 - Exception Handling
 - Validation
 - Pagination and Sorting
 - Java 11
 - Maven execution
 - In-memory database for development
 - Unique keys and Indexes on columns 
 
 
![picture](db-design.png)


## Setup Notes

**Run Project**
```sh
mvn spring-boot:run -Dspring-boot.run.profiles=test
``` 
**Run Tests**
```sh
mvn clean test
```

## API
I have [created Postman collection](CarAdPosting.postman_collection.json) for APIs. Please download the file and [import it in Postman](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/#importing-data-into-postman) to try APIs.

## Docker Setup
Docker file is also added in the project. Run the below commands in terminal to deploy project inside the docker container.

 ```sh
 mvn clean install
 ```

 ```sh
 docker build -t car-booking .
 ```

```sh
 docker run -p 8090:8080 car-booking
 ```
 
## Considerations
- As said in email there are many things that can be improved 
- Could have used [Immutable class generation](https://immutables.github.io/) if I had more time
- Could have used separate Models for Request and Response to avoid irrelevant data
- Used ORM for this project mainly to save some time but it also depends on use-case where ORMs are good or bad
- Could have used Spring Security but that was also time taking and out of scope
- Only added test for [UserService](src/test/java/com/car/rental/service/UserServiceImplTest.java)
- There can be many others reports that I did not included and their can be more business logic that I didnt cover but I did add some business logic in Services