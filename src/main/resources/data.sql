-- This file populates testing data in H2 database

INSERT INTO `user` (id,username,role) VALUES ('ac530dba-4bdf-494a-b66c-26fa1efa62d9','user_1','USER');
INSERT INTO `user` (id,username,role) VALUES ('2d3d39e4-7746-4784-95a3-848644be2d28','user_2','USER');
INSERT INTO `user` (id,username,role) VALUES ('804ec4eb-1b35-4696-b50a-f9b3f2223714','user_3','USER');
INSERT INTO `user` (id,username,role) VALUES ('4ce2f593-a27e-4a12-b5e4-c4c231d4c378','user_4','USER');
INSERT INTO `user` (id,username,role) VALUES ('30c1b18e-279f-49a5-8379-47769db9fbc0','user_5','USER');
INSERT INTO `user` (id,username,role) VALUES ('888958fe-2e70-4458-9d93-29ed17da5184','admin_1','ADMIN');
INSERT INTO `user` (id,username,role) VALUES ('67027189-6d02-41f7-a154-d780d3581df0','admin_2','ADMIN');

INSERT INTO `car` (id,plate_no,make,model,`year`) VALUES ('02e29d5f-d84e-45c6-998a-697f8cdb533b','ABC-123','Honda','Civic','2020');
INSERT INTO `car` (id,plate_no,make,model,`year`) VALUES ('884fc104-8e05-40c8-a9bd-1367d5a7e249','DEF-456','Toyota','Corolla','2019');
INSERT INTO `car` (id,plate_no,make,model,`year`) VALUES ('a0b19102-c4c1-4442-860a-497211c89244','GHI-789','Tesla','Model S','2018');

INSERT INTO `advertisement` (id,car_id,available_from,available_till,hourly_rate) VALUES ('825c805c-ecea-417a-9ec4-c3951eff3207','02e29d5f-d84e-45c6-998a-697f8cdb533b','2020-11-01 03:00:01','2020-11-05 00:00:00',16.5);
INSERT INTO `advertisement` (id,car_id,available_from,available_till,hourly_rate) VALUES ('16b09866-869a-406c-a8e4-d99e7fb088c9','884fc104-8e05-40c8-a9bd-1367d5a7e249','2020-11-01 06:00:01','2020-11-05 00:00:00',15.5);
INSERT INTO `advertisement` (id,car_id,available_from,available_till,hourly_rate) VALUES ('70c403c3-aa34-4a61-a27d-4844451d9abe','02e29d5f-d84e-45c6-998a-697f8cdb533b','2020-11-07 00:00:01','2020-11-08 00:00:00',18.5);
INSERT INTO `advertisement` (id,car_id,available_from,available_till,hourly_rate) VALUES ('825c8d05c-ecea-417a-9ec4-c3951eff3207','02e29d5f-d84e-45c6-998a-697f8cdb533b','2020-11-08 03:00:01','2020-11-09 00:00:00',16.5);
INSERT INTO `advertisement` (id,car_id,available_from,available_till,hourly_rate) VALUES ('16b0s9866-869a-406c-a8e4-d99e7fb088c9','884fc104-8e05-40c8-a9bd-1367d5a7e249','2020-11-10 06:00:01','2020-11-12 00:00:00',15.5);
INSERT INTO `advertisement` (id,car_id,available_from,available_till,hourly_rate) VALUES ('70ca403c3-aa34-4a61-a27d-4844451d9abe','02e29d5f-d84e-45c6-998a-697f8cdb533b','2020-11-13 00:00:01','2020-11-15 00:00:00',18.5);

INSERT INTO `booking` (id,user_id,advertisement_id,book_from,book_till,price) VALUES ('57cb66df-94c4-46da-9e63-8d628910263d','ac530dba-4bdf-494a-b66c-26fa1efa62d9', '825c805c-ecea-417a-9ec4-c3951eff3207', '2020-11-01 00:00:00','2020-11-01 03:00:00',49.5);
INSERT INTO `booking` (id,user_id,advertisement_id,book_from,book_till,price) VALUES ('d01a2194-fc7d-4818-9f03-9a5218b8b2c4','2d3d39e4-7746-4784-95a3-848644be2d28', '16b09866-869a-406c-a8e4-d99e7fb088c9', '2020-11-01 00:00:00','2020-11-01 06:00:00',93.0);
INSERT INTO `booking` (id,user_id,advertisement_id,book_from,book_till,price) VALUES ('e36e5732-908e-4e09-9815-75d0688edfb9','4ce2f593-a27e-4a12-b5e4-c4c231d4c378', '70c403c3-aa34-4a61-a27d-4844451d9abe', '2020-11-06 00:00:00','2020-11-07 00:00:00',444.0);
