package com.car.rental.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "booking",
        indexes = {@Index(name = "booking_bookFrom_index", columnList = "bookFrom", unique = false),
                @Index(name = "booking_bookTill_index", columnList = "bookTill", unique = false)},
        uniqueConstraints = @UniqueConstraint(columnNames = {"advertisement_id", "bookFrom", "bookTill"}))
public class Booking {
    @Id
    private String id;

    @NotNull(message = "BookFrom cannot be Null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bookFrom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message = "BookTill cannot be Null")
    private Date bookTill;

    private double price;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "advertisement_id")
    private Advertisement advertisement;

    @Transient
    private String bookingUserId;
    @Transient
    private String adId;

    public Booking(Date bookFrom, Date bookTill, double price, Advertisement ad, User user) {
        this.id = UUID.randomUUID().toString();
        this.bookFrom = bookFrom;
        this.bookTill = bookTill;
        this.price = price;
        this.advertisement = ad;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public Advertisement getAdvertisement() {
        return advertisement;
    }

    public Date getBookFrom() {
        return bookFrom;
    }

    public Date getBookTill() {
        return bookTill;
    }

    public User getUser() {
        return user;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBookingUserId() {
        return bookingUserId;
    }

    public String getAdId() {
        return adId;
    }

    // Required for Spring Data JPA
    public Booking() {
    }
}
