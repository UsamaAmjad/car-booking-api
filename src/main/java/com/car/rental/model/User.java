package com.car.rental.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Table(name = "user", indexes = {@Index(name = "user_role_index", columnList = "role", unique = false)})
public class User {
    @Id
    private String id;

    @Column(unique = true)
    @Length(min = 3, max = 30, message = "Username can be 3 to 30 characters long")
    @NotNull(message = "Please provide a Username")
    private String username;

    // Keeping it simple, otherwise this should be handled by proper Role-Perm module
    private String role;

    // Constructor based initialization to keep the Object immutable, only getters
    public User(String username, String role) {
        this.id = UUID.randomUUID().toString();
        this.username = username;
        this.role = role;
    }

    // User with Default Role
    public User(String username) {
        this.id = UUID.randomUUID().toString();
        this.username = username;
        this.role = "USER";
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }

    // Required for Spring Data JPA
    public User() {
    }
}
