package com.car.rental.model;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "advertisement",
        indexes = {@Index(name = "advertisement_availableFrom_index", columnList = "availableFrom", unique = false),
                @Index(name = "advertisement_availableTill_index", columnList = "availableTill", unique = false),
                @Index(name = "advertisement_hourlyRate_index", columnList = "hourlyRate", unique = false)},
        uniqueConstraints = @UniqueConstraint(columnNames = {"car_id", "availableFrom", "availableTill"}))
public class Advertisement {
    @Id
    private String id;

    @NotNull(message = "AvailableFrom cannot be Null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date availableFrom;
    @NotNull(message = "AvailableTill cannot be Null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date availableTill;
    @NotNull(message = "HourlyRate cannot be Null")
    private double hourlyRate;

    @Transient
    private String plateNo;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    public Advertisement(Date availableFrom, Date availableTill, double hourlyRate, Car car) {
        this.id = UUID.randomUUID().toString();
        this.availableFrom = availableFrom;
        this.availableTill = availableTill;
        this.hourlyRate = hourlyRate;
        this.car = car;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getAvailableFrom() {
        return availableFrom;
    }

    public Date getAvailableTill() {
        return availableTill;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    // Required for Spring Data JPA
    public Advertisement() {
    }
}
