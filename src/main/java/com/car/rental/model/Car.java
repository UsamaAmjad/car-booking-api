package com.car.rental.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Entity
@Table(name = "car", indexes = {@Index(name = "car_plate_index", columnList = "plateNo", unique = true)})
public class Car {
    @Id
    private String id;

    @Length(min = 3, max = 10, message = "Plate Number can be 3 to 10 characters long")
    private String plateNo;

    // There can be many other car details
    @NotEmpty(message = "Please provide a Make")
    private String make;
    @NotEmpty(message = "Please provide a Model")
    private String model;
    @NotEmpty(message = "Please provide a Year")
    private String year;

    public Car(String plateNo, String make, String model, String year) {
        this.id = UUID.randomUUID().toString();
        this.plateNo = plateNo;
        this.make = make;
        this.model = model;
        this.year = year;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getYear() {
        return year;
    }

    // Required for Spring Data JPA
    public Car() {
    }
}
