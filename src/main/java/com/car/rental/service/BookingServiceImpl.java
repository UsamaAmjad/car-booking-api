package com.car.rental.service;

import com.car.rental.model.Advertisement;
import com.car.rental.model.Booking;
import com.car.rental.repository.BookingRepo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Locale;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    private BookingRepo bookingRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private AdvertisementService advertService;

    @Override
    public Booking save(Booking book) {
        final Advertisement advert = advertService.getById(book.getAdId());
        final Instant bookStart = book.getBookFrom().toInstant();
        final Instant bookEnd = book.getBookTill().toInstant();
        final Instant advertStart = advert.getAvailableFrom().toInstant();
        final Instant advertEnd = advert.getAvailableFrom().toInstant();

        validateBookingTime(bookStart, bookEnd, advertStart, advertEnd);

        final Booking booking = new Booking(
                book.getBookFrom(),
                book.getBookTill(),
                calculateHours(bookStart, bookEnd) * advert.getHourlyRate(),
                advert,
                userService.getById(book.getBookingUserId())
        );

        return bookingRepo.save(booking);
    }

    @Override
    public Page<Booking> list(int pageNo, int pageSize, String sortBy, String from, String till) {
        final Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        if (Strings.isBlank(from) || Strings.isBlank(from)) {
            return bookingRepo.findAll(paging);
        }

        try {
            return bookingRepo.findAllByBookFromGreaterThanEqualAndBookTillLessThanEqual(format.parse(from), format.parse(till), paging);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid timestamp format");
        }
    }

    /*
     * This method could be improve in many ways such as
     * 1) I already applied a Unique constraint on (advertId,startTime,endTime) which means
     * users wont be able to book a car on exactly same time as other users but
     * 2) It should check if the car is already booked during certain time to avoid overlapping bookings
     * */
    private void validateBookingTime(Instant bookStart, Instant bookEnd, Instant advertStart, Instant advertEnd) {
        if ((bookStart.equals(advertStart) || bookStart.isAfter(advertStart))
                && bookStart.isBefore(advertEnd)) {
            throw new IllegalArgumentException("Invalid booking start time");
        }

        if (bookEnd.isAfter(advertStart)
                && (bookEnd.isBefore(advertEnd) || bookEnd.equals(advertEnd))) {
            throw new IllegalArgumentException("Invalid booking end time");
        }
    }

    /*
     * We could have more checks here if we want to cap user maximum or minimum booking times
     * */
    private double calculateHours(Instant bookStart, Instant bookEnd) {
        Duration duration = Duration.between(bookStart, bookEnd);
        if (duration.toHours() < 1) {
            throw new IllegalArgumentException("Booking should be at least 1 hour");
        }

        return duration.toHours();
    }
}
