package com.car.rental.service;

import com.car.rental.model.User;
import com.car.rental.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepo userRepo;

    @Override
    public User save(User usr) {
        return userRepo.save(new User(usr.getUsername()));
    }

    @Override
    public User getById(String id) {
        if (id == null || id == "") {
            throw new IllegalArgumentException("User ID can not be empty");
        }
        final Optional<User> user = userRepo.findById(id);
        if (user.isEmpty()) {
            throw new EntityNotFoundException("User NOT found");
        }

        return user.get();
    }
}
