package com.car.rental.service;

import com.car.rental.model.Booking;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public interface BookingService {
    Booking save(Booking booking);

    Page<Booking> list(int pageNo, int pageSize, String sortBy, String from, String till);
}
