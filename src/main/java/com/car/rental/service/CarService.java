package com.car.rental.service;

import com.car.rental.model.Car;

public interface CarService {
    Car save(Car car);

    Car getById(String id);

    Car getByPlateNo(String plateNo);
}
