package com.car.rental.service;

import com.car.rental.model.User;

public interface UserService {
    User save(User user);

    User getById(String id);
}
