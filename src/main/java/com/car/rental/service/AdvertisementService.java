package com.car.rental.service;

import com.car.rental.model.Advertisement;
import org.springframework.data.domain.Page;

public interface AdvertisementService {
    Advertisement save(Advertisement advert);

    Advertisement getById(String id);

    Page<Advertisement> list(int pageNo, int pageSize, String sortBy);
}
