package com.car.rental.service;

import com.car.rental.model.Advertisement;
import com.car.rental.repository.AdvertisementRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class AdvertisementServiceImpl implements AdvertisementService {
    @Autowired
    private AdvertisementRepo advertisementRepo;
    @Autowired
    private CarService carService;


    @Override
    public Advertisement save(Advertisement ad) {
        final Advertisement advert = new Advertisement(
                ad.getAvailableFrom(),
                ad.getAvailableTill(),
                ad.getHourlyRate(),
                carService.getByPlateNo(ad.getPlateNo())
        );
        ad = advertisementRepo.save(advert);
        return ad;
    }

    @Override
    public Page<Advertisement> list(int pageNo, int pageSize, String sortBy) {
        final Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<Advertisement> pagedResult = advertisementRepo.findAll(paging);
        if (pagedResult.hasContent()) {
            return pagedResult;
        } else {
            return pagedResult;
        }
    }

    @Override
    public Advertisement getById(String id) {
        if (id == null || id == "") {
            throw new IllegalArgumentException("Advert ID can not be empty");
        }
        final Optional<Advertisement> advert = advertisementRepo.findById(id);
        if (advert.isEmpty()) {
            throw new EntityNotFoundException("Advert NOT found");
        }

        return advert.get();
    }
}
