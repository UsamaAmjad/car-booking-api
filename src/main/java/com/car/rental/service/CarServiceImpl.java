package com.car.rental.service;

import com.car.rental.model.Car;
import com.car.rental.repository.CarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private CarRepo carRepo;

    @Override
    public Car save(Car car) {
        car = new Car(
                car.getPlateNo(),
                car.getMake(),
                car.getModel(),
                car.getYear()
        );
        return carRepo.save(car);
    }

    @Override
    public Car getById(String id) {
        if (id == null || id == "") {
            throw new IllegalArgumentException("Car ID can not be empty");
        }
        final Optional<Car> car = carRepo.findById(id);
        if (car.isEmpty()) {
            throw new EntityNotFoundException("Car NOT found");
        }

        return car.get();
    }

    @Override
    public Car getByPlateNo(String plateNo) {
        if (plateNo == null || plateNo == "") {
            throw new IllegalArgumentException("Car PlateNo can not be empty");
        }
        final Optional<Car> car = carRepo.findByPlateNo(plateNo);
        if (car.isEmpty()) {
            throw new EntityNotFoundException("Car NOT found");
        }

        return car.get();
    }
}
