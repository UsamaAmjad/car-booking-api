package com.car.rental.repository;

import com.car.rental.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends PagingAndSortingRepository<User, String> {
    Optional<User> findByUsername(String username);
}
