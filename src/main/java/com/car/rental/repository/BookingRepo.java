package com.car.rental.repository;

import com.car.rental.model.Booking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface BookingRepo extends PagingAndSortingRepository<Booking, String> {

    Page<Booking> findAllByBookFromGreaterThanEqualAndBookTillLessThanEqual(Date bookFrom, Date bookTill, Pageable pageable);

}
