package com.car.rental.repository;

import com.car.rental.model.Car;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarRepo extends PagingAndSortingRepository<Car, String> {
    Optional<Car> findByPlateNo(String plateNo);
}