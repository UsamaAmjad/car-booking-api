package com.car.rental.repository;

import com.car.rental.model.Advertisement;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvertisementRepo extends PagingAndSortingRepository<Advertisement, String> {
//    List<Advertisement> findByCar_PlateNo(String userId);
}
