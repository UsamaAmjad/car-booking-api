package com.car.rental.controller;

import com.car.rental.model.Booking;
import com.car.rental.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("bookings")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @PostMapping
    public ResponseEntity<Booking> saveBooking(@Valid @RequestBody Booking book) {

        final Booking booking = bookingService.save(book);

        // Adding this for completeness, GET endpoint is not implemented
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{bookingId}")
                .buildAndExpand(booking.getId()).toUri();

        return ResponseEntity.created(location).body(booking);
    }

    @GetMapping
    public ResponseEntity<Map<String, Object>> listBookings(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "10") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy,
                                                            @RequestParam(required = false) String from,
                                                            @RequestParam(required = false) String till) {

        final Page<Booking> bookingList = bookingService.list(pageNo, pageSize, sortBy, from, till);

        Map<String, Object> response = new HashMap<>();
        response.put("bookings", bookingList.getContent());
        response.put("currentPage", bookingList.getNumber());
        response.put("totalItems", bookingList.getTotalElements());
        response.put("totalPages", bookingList.getTotalPages());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
