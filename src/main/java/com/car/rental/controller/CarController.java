package com.car.rental.controller;

import com.car.rental.model.Car;
import com.car.rental.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("cars")
public class CarController {

    @Autowired
    private CarService carService;

    @PostMapping
    public ResponseEntity<Car> saveMessage(@Valid @RequestBody Car car) {
        car = carService.save(car);

        // Adding this for completeness, GET endpoint is not implemented
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{cars}")
                .buildAndExpand(car.getId()).toUri();

        return ResponseEntity.created(location).body(car);
    }

    //    There can be other Car endpoints such as Get, Update, List
}
