package com.car.rental.controller;

import com.car.rental.model.Advertisement;
import com.car.rental.service.AdvertisementService;
import com.car.rental.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("advertisements")
public class AdvertisementController {

    @Autowired
    private UserService userService;
    @Autowired
    private AdvertisementService advertisementService;

    @PostMapping
    public ResponseEntity<Advertisement> saveAdvert(@Valid @RequestBody Advertisement ad) {

        final Advertisement advertise = advertisementService.save(ad);

        // Adding this for completeness, GET endpoint is not implemented
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{advertisementId}")
                .buildAndExpand(advertise.getId()).toUri();

        return ResponseEntity.created(location).body(advertise);
    }

    @GetMapping
    public ResponseEntity<Map<String, Object>> listAdvert(@RequestParam(defaultValue = "0") Integer pageNo,
                                                          @RequestParam(defaultValue = "10") Integer pageSize,
                                                          @RequestParam(defaultValue = "id") String sortBy) {

        final Page<Advertisement> advertiseList = advertisementService.list(pageNo, pageSize, sortBy);

        Map<String, Object> response = new HashMap<>();
        response.put("advertisements", advertiseList.getContent());
        response.put("currentPage", advertiseList.getNumber());
        response.put("totalItems", advertiseList.getTotalElements());
        response.put("totalPages", advertiseList.getTotalPages());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //    There can be other Advertisement endpoints such as Get, Update, Delete
}
