package com.car.rental.service;

import com.car.rental.model.User;
import com.car.rental.repository.UserRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepo userRepo;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getByIdTest_Success() {
        final String userId = "mock-user";
        final User expected = new User(userId);
        when(userRepo.findById("mock-user")).thenReturn(Optional.of(expected));

        //test
        User response = userService.getById(userId);

        assertEquals(expected.getUsername(), response.getUsername());
        assertEquals(expected.getId(), response.getId());
        verify(userRepo, times(1)).findById(userId);
    }

    @Test
    public void getByIdTest_InvalidId() {
        //test
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            userService.getById("");
        });

        final String expectedMessage = "User ID can not be empty";
        final String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
        verify(userRepo, times(0)).findById("");
    }

    @Test
    public void getByIdTest_NotFound() {
        final String userId = "mock-user";
        when(userRepo.findById(userId)).thenReturn(Optional.empty());

        //test
        Exception exception = assertThrows(EntityNotFoundException.class, () -> {
            userService.getById(userId);
        });

        final String expectedMessage = "User NOT found";
        final String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
        verify(userRepo, times(1)).findById(userId);
    }

}