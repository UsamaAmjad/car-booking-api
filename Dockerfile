FROM adoptopenjdk:11-jre-hotspot
COPY ./target/rental-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch rental-0.0.1-SNAPSHOT.jar'
ENTRYPOINT ["java","-jar","rental-0.0.1-SNAPSHOT.jar"]